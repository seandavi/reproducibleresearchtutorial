This is a small repository containing a PDF of slides meant to give a bit of an overview of 
Reproducible Research and Literate Programming using R.  The slides are in Vignettes/ReproducibleResearch.pdf.
